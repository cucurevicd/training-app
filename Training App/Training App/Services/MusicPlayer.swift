//
//  MusicPlayer.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation

class MusicPlayer: NSObject {
    
    static let shared = MusicPlayer()
    
    @objc dynamic var mainPlayer : AVAudioPlayer?
    @objc dynamic var secondPlayer : AVAudioPlayer?
    var musicPlayer : MPMusicPlayerController?
    var currentSong = 0
    var playlist: [SongModel]?
    var loop: Bool = false
    
    func addPlaylist(playlist : [SongModel], autoplay: Bool, loop : Bool){
        
        currentSong = 0
        self.playlist = playlist
        
        if autoplay{
            playSong(self.playlist![currentSong])
        }
        
        self.loop = loop
    }
    
    private func playSong(_ song: SongModel){
        
        if let songURL = song.url{
            
            do{
                mainPlayer = try AVAudioPlayer(contentsOf: songURL)
                mainPlayer?.delegate = self
                mainPlayer?.enableRate = true
                mainPlayer?.prepareToPlay()
                mainPlayer?.play()
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func startPausePlayer(){
        
        if mainPlayer!.isPlaying{
            mainPlayer?.pause()
        }
        else{
            mainPlayer?.play()
        }
    }
    
    func stopPlayer(){
        
        if let player = mainPlayer{
            if player.isPlaying { player.stop() }
        }
        if let player = secondPlayer{
            if player.isPlaying { player.stop() }
        }
    }
    
    
    func playOtherSound(mediaItem : SongModel){
        
        if let songURL = mediaItem.url{
            
            do{
                secondPlayer = try AVAudioPlayer(contentsOf: songURL)
                secondPlayer?.prepareToPlay()
                secondPlayer?.play()
            }
            catch{
                print(error.localizedDescription)
            }
        }
    }
    
    func fadeSoundDown(duration: Double){
        
        mainPlayer?.setVolume(0.7, fadeDuration: duration)
        mainPlayer?.rate = 0.9
    }
    
    func fadeSoundUp(duration: Double){
        
        mainPlayer?.setVolume(1.0, fadeDuration: duration)
        mainPlayer?.rate = 1.0
    }
    
}

extension MusicPlayer : AVAudioPlayerDelegate{
    
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        
        guard mainPlayer != nil && self.playlist != nil && flag else {
            return
        }
        
        // Play next song
        if player == mainPlayer && currentSong != (self.playlist!.count - 1){
            currentSong += 1
            playSong(self.playlist![currentSong])
        }
        // Start from begining
        else if player == mainPlayer && currentSong == (self.playlist!.count - 1) && loop{
            currentSong = 0
            playSong(self.playlist![currentSong])
        }
    }
}
