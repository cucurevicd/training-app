//
//  MusicFileManager.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation
import MediaPlayer


class MusicFileManager {
    
    static func getMusic(musicItems: @escaping (_ items: [SongModel]?) -> Void ){
        
        requestAuthorization { (success) in
            
            guard success else{
                print("Enable access to your library to use this app")
                musicItems(nil)
                return
            }
            
            let songs = MPMediaQuery.songs().items?.map{ SongModel(media: $0) }
            musicItems(songs)
        }
    }
    
    static func getMusicFile(with name: String) -> SongModel?{
        
        if let url = Bundle.main.url(forResource: name, withExtension: "mp3"){

            let asset = AVURLAsset(url: url)
            let duration = Double(CMTimeGetSeconds(asset.duration))
            return SongModel(name: name, url: url, duration: duration)
        }
        return nil
    }
    
    private static func requestAuthorization(authorized: @escaping (_ success: Bool) -> Void){
        
        let status = MPMediaLibrary.authorizationStatus()
        
        switch status {
        case .authorized:
            authorized(true)
        case .notDetermined:
            MPMediaLibrary.requestAuthorization { (newStatus) in
                authorized(newStatus == .authorized)
            }
        default:
            authorized(false)
        }
    }
    
    
}
