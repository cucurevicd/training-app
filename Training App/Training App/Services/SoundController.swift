//
//  SoundController.swift
//  Training App
//
//  Created by Apple on 5/24/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation
import AVKit

enum WorkoutState : Int{
    case workout
    case workoutBreak
    case warmup
    case ended
    case paused
}

enum WorkoutStateNotification: String {
    case soundControllerStateDidChanged = "SoundControllerStateDidChanged"
    case soundControllerStateWillChange = "soundControllerStateWillChange"
    
    var notification: Notification.Name{
        return Notification.Name(rawValue: self.rawValue)
    }
}

class SoundController {
    
    static let shared = SoundController()
    var breakPoint: Double?{
        didSet{
            if breakPoint != nil{
                
                print(breakPoint)
                if self.breakPoint! < 1{
                    currentPart += 1
                    
                    if currentPart == self.queueParts.count{
                        stopWorkout()
                        return
                    }
                    handlePart(self.queueParts[currentPart])
                }
                
                self.handleTime(passTime: breakPoint!)
            }
        }
    }
    var queueParts = [IntervalTypeProtocol]()
    var playlist : [SongModel]?
    var state: WorkoutState = .warmup{
        didSet{
            NotificationCenter.default.post(name: WorkoutStateNotification.soundControllerStateDidChanged.notification , object: nil, userInfo: ["state" : state])
        }
        willSet{
            NotificationCenter.default.post(name: WorkoutStateNotification.soundControllerStateWillChange.notification , object: nil, userInfo: ["state" : state])
        }
    }
    var currentPart: Int = 0
    
    let startBreakSound = MusicFileManager.getMusicFile(with: "Start break sound")
    let endBreakSound = MusicFileManager.getMusicFile(with: "End break sound")
    let strartWorkoutSound = MusicFileManager.getMusicFile(with: "Start workout")
    var timer: Timer?
    
    var enableToPlaySecondSound : Bool{
        get{
            return MusicPlayer.shared.secondPlayer != nil ? !MusicPlayer.shared.secondPlayer!.isPlaying : true
        }
    }
    
    func startWorkout(_ workout : WorkoutModel, autoplayMusic: Bool = false, loop: Bool = false){
        
        // Play music
        if let playlist = workout.songs{
            MusicPlayer.shared.addPlaylist(playlist: playlist, autoplay: autoplayMusic, loop: loop)
        }
        
        // Observer music timer
        
        self.timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { [weak self] (timer) in
            
            if self?.breakPoint != nil{
                self?.breakPoint! -= 1.0
            }
        })
        
        // Add all intervals
        workout.intervals?.forEach{
            if let allParts = $0.allBreakExercise{
                let all = allParts.map{ $0 }
                queueParts.append(contentsOf: all)
            }
        }
        
        // Start workout after 5 sec
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.startInterval()
        }
    }
    
    func startInterval(){
        
        if !(self.timer?.isValid)!{
            self.timer?.fire()
        }
        beginWorkoutIntervals()
    }
    
    func stopWorkout() {
        
        self.timer?.invalidate()
        self.state = .ended
    }
    
    func stopInterval(){
        
        self.timer?.invalidate()
    }
    
    private func beforeBreakStart(time: Double){
        
        if startBreakSound != nil{
            
            if time <= startBreakSound?.duration ?? 1 && enableToPlaySecondSound{
                MusicPlayer.shared.playOtherSound(mediaItem: startBreakSound!)
                
                DispatchQueue.main.asyncAfter(deadline: .now() + (startBreakSound!.duration ?? 0)) { [weak self] in
                    MusicPlayer.shared.fadeSoundDown(duration: self?.startBreakSound!.duration ?? 0)
                }
            }
        }
    }
    
    private func beforeBreakEnd(time: Double){
        
        if endBreakSound != nil{
            
            if time <= endBreakSound!.duration ?? 1 && enableToPlaySecondSound{
                MusicPlayer.shared.playOtherSound(mediaItem: endBreakSound!)
                DispatchQueue.main.asyncAfter(deadline: .now() - 1 + (endBreakSound!.duration ?? 0)) { [weak self] in
                    MusicPlayer.shared.fadeSoundUp(duration: self?.endBreakSound!.duration ?? 0)
                }
            }
        }
    }
    
    private func beginWorkoutIntervals(){
    
        if let song = MusicFileManager.getMusicFile(with: "Start workout"){
            
            MusicPlayer.shared.fadeSoundDown(duration: song.duration ?? 0)
            MusicPlayer.shared.playOtherSound(mediaItem: song)
            
            DispatchQueue.main.asyncAfter(deadline: .now() + (song.duration ?? 0)) {
                
                MusicPlayer.shared.fadeSoundUp(duration: 0)
            }
        }
        
        handlePart(self.queueParts[currentPart])
    }
    
    private func workoutWillEnd(){
    
        print("Workout will end after this interval")
    }
    
    private func handleTime(passTime: Double){
    
        guard self.queueParts.count > currentPart + 1 else {
            workoutWillEnd()
            return
        }
        
        self.queueParts[currentPart + 1] is BreakModel ? beforeBreakStart(time: passTime) : beforeBreakEnd(time: passTime)
    }
    
    private func handlePart(_ part: IntervalTypeProtocol){
        
        self.breakPoint = part.duration
        self.state = part is ExerciseModel ? .workout : .workoutBreak
    }
}
