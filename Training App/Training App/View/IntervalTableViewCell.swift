//
//  IntervalTableViewCell.swift
//  Training App
//
//  Created by Apple on 5/29/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import UIKit

class IntervalTableViewCell: UITableViewCell {

    @IBOutlet weak var intervalDescription: UILabel!
    @IBOutlet weak var intervalCount: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    
    var interval: IntervalModel?{
        didSet{
            if interval != nil{
                configUI()
            }
        }
    }
    
    
    private func configUI(){
        
        var breakDuration = 0.0
        var exerciseDuration = 0.0
        
        let duration = interval?.allBreakExercise?.reduce(0, { (allDuration, part) -> Double in
            
            if let breakPart = part as? BreakModel{
                breakDuration = breakPart.duration ?? 0
            }
            else if let exercisePart = part as? ExerciseModel{
                exerciseDuration = exercisePart.duration ?? 0
            }
            
            return (part.duration ?? 0) + allDuration
        })

        self.durationLabel.text = "Duration: \(duration ?? 0)"
        
        self.intervalDescription.text = "Exercise duration: \(exerciseDuration)\nBreak duration: \(breakDuration)"
        
        if let repeatTime = interval?.repeatCount{
            self.intervalCount.text = "X\(repeatTime)"
        }
    }
}
