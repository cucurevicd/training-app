//
//  WorkoutDetailsViewController.swift
//  Training App
//
//  Created by Apple on 5/28/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import UIKit

class WorkoutDetailsViewController: UIViewController , UITextFieldDelegate{
    
    var workout: WorkoutModel?
    @IBOutlet weak var workoutName: UITextField!
    @IBOutlet weak var allIntervalsTableView: UITableView!
    @IBOutlet weak var workDurationTextField: UITextField!
    @IBOutlet weak var breakDurationTextField: UITextField!
    @IBOutlet weak var intervalsNumberTextField: UITextField!
    @IBOutlet weak var startWorkoutButton: UIButton!
    @IBOutlet weak var selectMusicButton: UIButton!
    
    var hour: Int = 0
    var minutes: Int = 0
    var seconds: Int = 0
    
    var workDuration: Double?
    var breakDuration: Double?
    var intervalCount: Int = 1
    
    var selectedTextField: UITextField?
    
    let picker = UIPickerView()
    
    var selectedSongs : [SongModel]?{
        didSet{
            self.startWorkoutButton.isEnabled = selectedSongs != nil
            let title = selectedSongs != nil ? "Select music" : "Edit playlist"
            self.selectMusicButton.setTitle(title, for: .normal)
        }
    }
    
    var intervals = [IntervalModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.workDurationTextField.delegate = self
        self.breakDurationTextField.delegate = self
        self.intervalsNumberTextField.delegate = self
        
        self.allIntervalsTableView.delegate = self
        self.allIntervalsTableView.dataSource = self
        allIntervalsTableView.register(UINib(nibName: "IntervalTableViewCell", bundle: nil), forCellReuseIdentifier: "IntervalTableViewCell")
        allIntervalsTableView.estimatedRowHeight = 80.0
        
        self.picker.delegate = self
        
    }
    
    //MARK:- Button actions
    @IBAction func addIntervalsAction(_ sender: UIButton) {
        
        guard workDurationTextField.text != "" else {
            let alert = UIAlertController(title: "Error", message: "Enter excersise duration", preferredStyle: .alert)
            let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alert.addAction(action)
            
            self.navigationController?.present(alert, animated: true, completion: {
                self.workDurationTextField.becomeFirstResponder()
            })
            
            return
        }
        
        var allParts = [IntervalTypeProtocol]()
        for _ in 0..<intervalCount{
            let breakModel = BreakModel(duration: breakDuration)
            let exerciseModel = ExerciseModel(duration: workDuration)
            allParts.append(exerciseModel)
            allParts.append(breakModel)
        }
        
        let interval = IntervalModel(allBreakExercise: allParts, repeatCount: intervalCount)
        
        if workout != nil{
            workout?.intervals?.append(interval)
        }
        else{
            intervals.append(interval)
        }
        
        self.workDurationTextField.text = ""
        self.breakDurationTextField.text = ""
        self.intervalsNumberTextField.text = ""
        
        self.resignFirstResponder()
        addIntervalCell()
    }
    
    private func parseTime(timeString: String) -> Double{
        
        let multiplier = 3600
        var counter = 1
        return timeString.components(separatedBy: ":").reduce(0) { (result, component) -> Double in
            
            let intValue = Int(component)
            let durationResult = intValue! * (multiplier / counter)
            counter = counter * 60
            return Double(durationResult) + result
        }
    }
    
    @IBAction func startWorkoutAction(_ sender: UIButton) {
        
        if workout != nil{
            self.performSegue(withIdentifier: "startWorkout", sender: nil)
        }
        else{
            
            do{
                let workout = try WorkoutModel(name: self.workoutName.text, intervals: intervals, songs: selectedSongs)
                self.workout = workout
                self.performSegue(withIdentifier: "startWorkout", sender: nil)
            }
            catch{
                errorHandle(error: error as! WorkoutInitError)
            }
        }
    }
    
    private func errorHandle(error: WorkoutInitError){
        
        var alertMessage: String?
        switch error {
        case .missingName:
            alertMessage = "Enter workout name"
        case .missingIntervals:
            alertMessage = "Add intervals"
        case .missingMusic:
            alertMessage = "Select music"
        }
        
        let alert = UIAlertController(title: "Missing fields", message: alertMessage, preferredStyle: .alert)
        let action = UIAlertAction(title: "OK", style: .cancel, handler: nil)
        alert.addAction(action)
        
        self.navigationController?.present(alert, animated: true, completion: nil)
    }
    
    //MARK:- Text field
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        self.selectedTextField = textField
        
        if textField != self.intervalsNumberTextField {
            textField.inputView = self.picker
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if textField == self.intervalsNumberTextField {
            self.intervalCount = Int(textField.text!) ?? 1
        }
        else{
            if textField == self.breakDurationTextField{
                breakDuration = parseTime(timeString: textField.text!)
            }
            else if textField == self.workDurationTextField{
                workDuration = parseTime(timeString: textField.text!)
            }
        }
    }

    
    //MARK:- Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? AllSongsTableViewController{
            
            vc.delegate = self
        }
        else if let dest = segue.destination as? WorkoutViewController{
            
            dest.workout = self.workout!
        }
    }
}

extension WorkoutDetailsViewController : UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return workout?.intervals?.count ?? intervals.count
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "IntervalTableViewCell", for: indexPath) as! IntervalTableViewCell
        
        let validIntervals = self.workout?.intervals ?? intervals
        let interval = validIntervals[indexPath.row]
        cell.interval = interval
        
        return cell
    }
    
    private func addIntervalCell(){
        
        allIntervalsTableView.beginUpdates()
        allIntervalsTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .automatic)
        allIntervalsTableView.endUpdates()
    }
}

extension WorkoutDetailsViewController : SongSelectionDelegate{
    
    func songsSelected(songs: [SongModel]) {
        self.selectedSongs = songs
    }
}

extension WorkoutDetailsViewController : UIPickerViewDelegate, UIPickerViewDataSource{
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        switch component {
        case 0:
            return 25
        case 1,2:
            return 60
            
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.frame.size.width/3
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch component {
        case 0:
            return "\(row) h"
        case 1:
            return "\(row) m"
        case 2:
            return "\(row) s"
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        switch component {
        case 0:
            hour = row
        case 1:
            minutes = row
        case 2:
            seconds = row
        default:
            break;
        }

        selectedTextField?.text = "\(hour):\(minutes):\(seconds)"
    }
}
