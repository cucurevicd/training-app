//
//  ViewController.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer

class ViewController: UIViewController {

    @IBOutlet weak var playSongButton: UIButton!
    var observer : NSKeyValueObservation?
    

    
    //MARK:- View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        NotificationCenter.default.addObserver(self, selector: #selector(ViewController.changeBackgroundColor(_:)), name: WorkoutStateNotification.soundControllerStateDidChanged.notification, object: nil)
    }
    
    @objc func changeBackgroundColor(_ notif: Notification){
       
        if let state = notif.userInfo!["state"] as? WorkoutState{
            
            switch state{
            
            case .warmup:
                 self.view.backgroundColor = UIColor.yellow
                break
            case .workout:
                self.view.backgroundColor = UIColor.red
                break
            case .workoutBreak:
                self.view.backgroundColor = UIColor.green
                break
            case .paused:
                self.view.backgroundColor = UIColor.blue
                break
            case .ended:
                self.view.backgroundColor = UIColor.cyan
                break
            }
        }
    }

    //MARK:- Button actions
    
    @IBAction func playSongAction(_ sender: UIButton) {
        MusicPlayer.shared.startPausePlayer()
    }
    
    @IBAction func playSoundAction(_ sender: UIButton) {
        
        MusicFileManager.getMusic { (items) in
            
            MusicPlayer.shared.playOtherSound(mediaItem: items!.first!)
        }
    }
    
    @IBAction func selectSongAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "allSongsList", sender: nil)
    }
    
    @IBAction func startWorkoutAction(_ sender: UIButton) {
        
        guard selectedSongs != nil else {
            let alert = UIAlertController(title: "Error", message: "Select music first", preferredStyle: .alert)
            self.navigationController?.present(alert, animated: true, completion: nil)

            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                _ = alert.popoverPresentationController
            }
            return
        }
        
        createWorkout()
    }
    
    private func createWorkout(){
    
        let excersise1 = ExerciseModel(duration: 15.0)
        let break1 = BreakModel(duration: 8.0)
        let interval1 = IntervalModel(allBreakExercise: [excersise1, break1])
        let interval2 = IntervalModel(allBreakExercise: [excersise1, break1])
        let workout = WorkoutModel(name: "Test", intervals: [interval1, interval2], songs: selectedSongs!)
        
        SoundController.shared.startWorkout(workout, autoplayMusic: true, loop: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let vc = segue.destination as? AllSongsTableViewController{
            
            vc.delegate = self
        }
    }
}

extension ViewController : SongSelectionDelegate{
    
    func songsSelected(songs: [SongModel]) {
        self.selectedSongs = songs
    }
}

