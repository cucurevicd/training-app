//
//  AllSongsTableViewController.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import UIKit

protocol SongSelectionDelegate {
    
    func songsSelected(songs: [SongModel])
}

class AllSongsTableViewController: UITableViewController {
    
    var songs: [SongModel]?{
        didSet{
            navigationItem.rightBarButtonItem?.isEnabled = songs != nil
        }
    }
    var selectedSongs = [SongModel]()
    var delegate: SongSelectionDelegate?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
        
        self.tableView.allowsMultipleSelection = true
        
        MusicFileManager.getMusic { [weak self] (items) in
            
            self?.songs = items
            DispatchQueue.main.async {
                self?.tableView.reloadData()
            }
        }
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(selectSongs))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return songs?.count ?? 0
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)

        cell.textLabel?.text = self.songs![indexPath.row].title

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
    }
    
    @objc private func selectSongs(){
        
        tableView.indexPathsForSelectedRows?.forEach{ selectedSongs.append(songs![$0.row]) }
        
        guard selectedSongs.count != 0 else {
            let alert = UIAlertController(title: "Error", message: "Please select at least one song!", preferredStyle: .alert)
            self.navigationController?.present(alert, animated: true, completion: nil)
            return
        }
        
        delegate?.songsSelected(songs: selectedSongs)
        self.navigationController?.popViewController(animated: true)
    }
}
