//
//  BreakModel.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation

struct BreakModel : IntervalTypeProtocol {

    var duration: Double?
}
