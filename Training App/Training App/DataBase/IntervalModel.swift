//
//  IntervalModel.swift
//  Training App
//
//  Created by Apple on 5/24/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation

protocol IntervalTypeProtocol {
    var duration: Double? {get set}
}

struct IntervalModel{
    
    var allBreakExercise : [IntervalTypeProtocol]?
    var repeatCount: Int?
}
