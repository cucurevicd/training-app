//
//  SongModel.swift
//  Training App
//
//  Created by Apple on 5/23/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation
import MediaPlayer

struct SongModel {
    
    init(media: MPMediaItem) {
        
        type =  media.value(forProperty: MPMediaItemPropertyMediaType) as? String
        title = media.value(forProperty: MPMediaItemPropertyTitle) as? String
        albumTitle = media.value(forProperty: MPMediaItemPropertyAlbumTitle) as? String
        artist = media.value(forProperty: MPMediaItemPropertyArtist) as? String
        bpm = media.value(forProperty: MPMediaItemPropertyBeatsPerMinute) as? String
        url = media.value(forProperty: MPMediaItemPropertyAssetURL) as? URL
        duration = media.value(forProperty: MPMediaItemPropertyPlaybackDuration) as? Double
    }
    
    init(name: String, url: URL, duration: Double?) {
        
        self.title = name
        self.url = url
        self.duration = duration
    }
    
    var type : String?
    var title : String?
    var albumTitle : String?
    var artist: String?
    var bpm: String?
    var url: URL?
    var duration: Double?
}
