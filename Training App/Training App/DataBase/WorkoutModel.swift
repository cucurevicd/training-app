//
//  WorkoutModel.swift
//  Training App
//
//  Created by Apple on 5/24/18.
//  Copyright © 2018 Quantox. All rights reserved.
//

import Foundation

enum WorkoutInitError : Error{
    
    case missingName
    case missingIntervals
    case missingMusic
}

struct WorkoutModel {
    
    var name: String?
    var intervals: [IntervalModel]?
    var songs: [SongModel]?
    
    init(name: String?, intervals: [IntervalModel]?, songs: [SongModel]?) throws{
        
        if name == nil{
            throw WorkoutInitError.missingName
        }
        else if name!.isEmpty{
            throw WorkoutInitError.missingName
        }
        else if intervals == nil{
            throw WorkoutInitError.missingIntervals
        }
        else if intervals!.isEmpty{
            throw WorkoutInitError.missingIntervals
        }
        else if songs == nil{
            throw WorkoutInitError.missingMusic
        }
        else if songs!.isEmpty{
            throw WorkoutInitError.missingMusic
        }
        
        self.name = name
        self.intervals = intervals
        self.songs = songs
    }
}

extension WorkoutModel{
    
    var duration: Double{
        get{
            
            guard intervals != nil else{
                return 0
            }
            
            return intervals!.reduce(0) { (res, interval) -> Double in
                
                guard interval.allBreakExercise != nil else{
                    return 0
                }
                
                return interval.allBreakExercise!.reduce(0, { (intervalResult, execrsiseBreakModel) -> Double in
                    
                    if let modelExercise = execrsiseBreakModel as? BreakModel{
                        return modelExercise.duration ?? 0 + intervalResult
                    }
                    else if let modelBreak = execrsiseBreakModel as? ExerciseModel{
                        return modelBreak.duration ?? 0 + intervalResult
                    }
                    return 0
                })
            }
        }
    }
}
